package Interface;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class EditReservation {
    private JPanel mainPanel;
    private JTextField nameField;
    private JButton plusButton;
    private JButton minusButton;
    private JLabel nrSeats;
    private JButton saveButton;

    private String originalName;
    private int nrSeatsValue;

    public JPanel getMainPanel(){return mainPanel;}

    public EditReservation(String nameReservation, int nrSeats){

        this();

        nameField.setText(nameReservation);
        nrSeatsValue = nrSeats;
        originalName = nameReservation;
        this.nrSeats.setText(String.valueOf(nrSeats));
    }

    public EditReservation() {

        plusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                nrSeatsValue++;
                nrSeats.setText(String.valueOf(nrSeatsValue));
            }
        });
        minusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if(nrSeatsValue - 1 > 0){

                    nrSeatsValue--;
                    nrSeats.setText(String.valueOf(nrSeatsValue));
                }
            }
        });
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String newName = nameField.getText();

                if(newName.equals(""))
                    UserInterface.showMessage(UserInterface.editReservationFrame, "Invalid name");
                else{

                    try{

                        UserInterface.getBll().editReservation(originalName, newName, nrSeatsValue);

                    }catch (IOException exc) {
                        UserInterface.showMessage(UserInterface.editReservationFrame, exc.getMessage());
                    }
                }
            }
        });
    }
}
