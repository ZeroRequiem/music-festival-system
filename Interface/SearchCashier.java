package Interface;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;

public class SearchCashier {

    private JTextField cashierName;
    private JButton editButton;
    private JButton search;
    private JButton deleteButton;
    private JPanel mainPanel;
    private JList listCashiers;
    private JScrollPane cashierScroll;
    private JPanel cashiersPanel;
    private JLabel name;

    private DefaultListModel<String> listModel = new DefaultListModel<String>();

    public SearchCashier() {

        search.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String[] resultList = UserInterface.getBll().searchCashier(cashierName.getText());
                listModel.removeAllElements();

                for(String curr: resultList)
                    listModel.addElement(curr);

                listCashiers.setModel(listModel);
                cashierScroll.setViewportView(listCashiers);
            }
        });
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String name = (String)listCashiers.getSelectedValue();

                if(name == null || name.equals("Empty"))
                    UserInterface.showMessage(UserInterface.searchCashierFrame, "Not a valid selection");
                else
                    UserInterface.openTemporaryWindow(UserInterface.editCashierFrame,
                                                        new EditCashier(name, UserInterface.getBll().getPassword(name))
                                                                                            .getMainPanel());

            }
        });
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String name = (String)listCashiers.getSelectedValue();

                if(name == null || name.equals("Empty"))
                    UserInterface.showMessage(UserInterface.searchCashierFrame, "Not a valid selection");
                else {

                    UserInterface.getBll().deleteCashier(name);
                    UserInterface.showMessage(UserInterface.searchCashierFrame, "Deleted cashier: " + name);
                    listModel.setElementAt("Empty", 0);

                }
            }
        });
    }

    public JPanel getMainPanel(){return mainPanel;}

}
