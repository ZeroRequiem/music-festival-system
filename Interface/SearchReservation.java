package Interface;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SearchReservation {
    private JPanel mainPanel;
    private JTextField reservationName;
    private JButton searchButton;
    private JScrollPane reservationScroll;
    private JList listReservations;
    private JButton editButton;
    private JButton deleteButton;

    private DefaultListModel<String> listModel = new DefaultListModel<String>();
    
    public JPanel getMainPanel(){return mainPanel;}

    public SearchReservation() {
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String[] resultList = UserInterface.getBll().searchReservation(reservationName.getText());
                listModel.removeAllElements();

                for(String curr: resultList)
                    listModel.addElement(curr);

                listReservations.setModel(listModel);
                reservationScroll.setViewportView(listReservations);
            }
        });
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String name = (String) listReservations.getSelectedValue();

                if(name == null || name.equals("Empty"))
                    UserInterface.showMessage(UserInterface.searchReservation, "Invalid selection");
                else {

                    int nrSeats = UserInterface.getBll().getReservation(name).getNumberOfSeats();
                    UserInterface.openTemporaryWindow(UserInterface.editReservationFrame, new EditReservation(name, nrSeats).getMainPanel());
                }
            }
        });
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String name = (String) listReservations.getSelectedValue();

                if(name == null || name.equals("Empty"))
                    UserInterface.showMessage(UserInterface.searchReservation, "Invalid selection");
                else {

                    UserInterface.getBll().deleteReservation(name);
                    UserInterface.showMessage(UserInterface.searchReservation, "Deleted reservation with name: " + name);
                }
            }
        });
    }
}
