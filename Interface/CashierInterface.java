package Interface;

import Model.Shows;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class CashierInterface {

    private JPanel mainPanel;
    private JButton sellTicketsButton;
    private JButton searchReservationButton;
    private JButton showListOfReservationsButton;
    private JTextField showNameField;

    public CashierInterface() {

        sellTicketsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                UserInterface.openTemporaryWindow(UserInterface.sellTicketFrame, new SellTicket().getMainPanel());
            }
        });

        searchReservationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                UserInterface.openTemporaryWindow(UserInterface.searchReservation, new SearchReservation().getMainPanel());
            }
        });
        showListOfReservationsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String showName = showNameField.getText();

                if(showName.equals(""))
                    UserInterface.showMessage(UserInterface.cashierFrame, "Invalid show name!");
                else{

                    try{

                        Shows show = UserInterface.getBll().findShow(showName);
                        UserInterface.openTemporaryWindow(UserInterface.listReservations, new ListReservations(show.getTitle()).getMainPanel());

                    }catch (IOException exc) {
                        UserInterface.showMessage(UserInterface.cashierFrame, exc.getMessage());
                    }

                }
            }
        });
    }

    public JPanel getMainPanel(){return mainPanel;}


}
