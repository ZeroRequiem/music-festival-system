package Interface;

import javax.swing.*;
import java.io.IOException;

public class ListReservations {

    private JPanel mainPanel;
    private JList listReservations;
    private JLabel nameShowLabel;
    private JScrollPane scrollRes;

    DefaultListModel<String> modelListRes = new DefaultListModel<String>();

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public ListReservations(){}

    public ListReservations(String showName){

        nameShowLabel.setText(showName);

        try{

            String[] resultList = UserInterface.getBll().getReservationsForShow(showName);
            modelListRes.removeAllElements();

            for(String curr: resultList)
                modelListRes.addElement(curr);

            listReservations.setModel(modelListRes);
            scrollRes.setViewportView(listReservations);

        }catch (IOException exc){
            UserInterface.showMessage(UserInterface.listReservations, exc.getMessage());
        }
    }
}
