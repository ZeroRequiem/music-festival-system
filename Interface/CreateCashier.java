package Interface;

import BLL.BLL;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CreateCashier {
    private JTextField cashierName;
    private JButton createButton;
    private JTextField passwordField;
    private JPanel panel1;
    private BLL bll = new BLL();

    public CreateCashier() {

        createButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                UserInterface.getBll().createCashier(cashierName.getText(), passwordField.getText());
                UserInterface.showMessage(UserInterface.createCashierFrame, "Successfully created cashier with name: " + cashierName.getText());
            }
        });
    }

    public JPanel getMainPanel(){return panel1;}
}
