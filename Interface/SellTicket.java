package Interface;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class SellTicket {
    private JPanel mainPanel;
    private JTextField showName;
    private JButton searchButton;
    private JScrollPane showScroll;
    private JList listShows;
    private JButton plusButton;
    private JButton minusButton;
    private JLabel seatsField;
    private JTextField reservationNameField;
    private JButton saveButton;

    private DefaultListModel<String> listModel = new DefaultListModel<String>();
    private int numberOfSeats;

    public JPanel getMainPanel(){return mainPanel;}

    public SellTicket() {

        numberOfSeats = 1;
        seatsField.setText("1");
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String[] resultList = UserInterface.getBll().searchShow(showName.getText());
                listModel.removeAllElements();

                for(String curr: resultList)
                    listModel.addElement(curr);

                listShows.setModel(listModel);
                showScroll.setViewportView(listShows);
            }
        });
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String title = (String) listShows.getSelectedValue();
                String reservationName = reservationNameField.getText();
                int nrSeats = Integer.parseInt(seatsField.getText());

                if(title == null || title.equals("Empty"))
                    UserInterface.showMessage(UserInterface.sellTicketFrame, "Invalid title");
                else if(reservationName.equals(""))
                    UserInterface.showMessage(UserInterface.sellTicketFrame, "Invalid reservation name");
                else {

                    try {

                        UserInterface.getBll().addReservation(title, reservationName, nrSeats);
                        UserInterface.showMessage(UserInterface.sellTicketFrame, "Added reservation: " + reservationName);

                    }catch (IOException exc){
                        UserInterface.showMessage(UserInterface.sellTicketFrame, exc.getMessage());
                    }
                }
            }
        });
        plusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                numberOfSeats++;
                seatsField.setText(String.valueOf(numberOfSeats));
            }
        });
        minusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if(numberOfSeats - 1 > 0){

                    numberOfSeats--;
                    seatsField.setText(String.valueOf(numberOfSeats));
                }
            }
        });
    }
}
