package Interface;

import Auxi.Status;
import BLL.BLL;
import oracle.jrockit.jfr.JFR;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.IOException;

public class UserInterface {

    private static JFrame loginFrame = new JFrame("Login");
    private static JFrame adminFrame = new JFrame("Admin");

    public static JFrame cashierFrame = new JFrame("Cashier");
    public static JFrame createCashierFrame = new JFrame("Create Cashier");
    public static JFrame searchCashierFrame = new JFrame("Search cashier");
    public static JFrame editCashierFrame = new JFrame("Edit Cashier");

    public static JFrame createShowFrame = new JFrame("Create Show");
    public static JFrame searchShowFrame = new JFrame("Search Show");
    public static JFrame editShowFrame = new JFrame("Edit show");

    public static JFrame sellTicketFrame = new JFrame("Sell tickets");
    public static JFrame searchReservation = new JFrame("Search reservation");
    public static JFrame editReservationFrame = new JFrame("Edit reservation");
    public static JFrame listReservations = new JFrame("List reservations");

    private JPanel panel1;
    private JTextField userNameField;
    private JPasswordField passwordField;
    private JButton Login;

    private static BLL bll = new BLL();

    public static void showMessage(JFrame frame, String msg){

        JOptionPane.showMessageDialog(frame, msg);
    }

    public static BLL getBll(){return bll;}

    public static void openWindow(JFrame frame, JPanel mainPanel){

        frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.pack();
        frame.setVisible(true);
    }

    public static void closeWindow(JFrame frame){

        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
    }

    public static void openTemporaryWindow(JFrame frame, JPanel mainPanel){

        frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        frame.pack();
        frame.setVisible(true);
    }

    public static void hideWindow(JFrame frame){

        frame.setVisible(false);
    }

    public UserInterface() {
        Login.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String username = userNameField.getText();
                char[] password = passwordField.getPassword();

                try{

                    int userStatus = bll.getUserStatus(username, new String(password));
                    hideWindow(loginFrame);

                    if(userStatus == Status.ADMIN)
                        openWindow(adminFrame, new AdminInterface().getPanel1());
                    else
                        openWindow(cashierFrame, new CashierInterface().getMainPanel());

                }catch(IOException exc){

                    showMessage(loginFrame, exc.getMessage());
                    userNameField.setText("");
                    passwordField.setText("");
                }
            }
        });
    }

    public static void refreshFrame(JFrame frame){

        frame.validate();
        frame.repaint();
    }

    public static void main(String[] args) {

       openWindow(loginFrame, new UserInterface().panel1);
    }
}
