package Interface;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EditCashier {

    private JTextField nameField;
    private JButton saveButton;
    private JPanel mainPanel;
    private JTextField passwordField;
    private String nameValue;

    public EditCashier(){

        this("", "");
    }

    public EditCashier(String name, String password) {

        nameField.setText(name);
        passwordField.setText(UserInterface.getBll().decode(password));
        nameValue = name;

        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                UserInterface.getBll().editCashier(nameValue, nameField.getText(), passwordField.getText());
                UserInterface.showMessage(UserInterface.editCashierFrame, "Updated cashier info!");
            }
        });
    }

    public JPanel getMainPanel(){return mainPanel;}
}
