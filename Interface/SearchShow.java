package Interface;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SearchShow {
    
    private JPanel mainPanel;
    private JTextField showName;
    private JButton searchButton;
    private JButton editButton;
    private JButton deleteButton;
    private JList listShows;
    private JScrollPane showScroll;
    private JButton JSONFormatButton;
    private JButton CSVFormatButton;

    private DefaultListModel<String> listModel = new DefaultListModel<String>();
    
    public SearchShow(){

        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String[] resultList = UserInterface.getBll().searchShow(showName.getText());
                listModel.removeAllElements();

                for(String curr: resultList)
                    listModel.addElement(curr);

                listShows.setModel(listModel);
                showScroll.setViewportView(listShows);
            }
        });
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String title = (String)listShows.getSelectedValue();

                if(title == null || title.equals("Empty"))
                    UserInterface.showMessage(UserInterface.searchShowFrame, "Not a valid selection");
                else
                    UserInterface.openTemporaryWindow(UserInterface.editShowFrame,
                            new EditShow(UserInterface.getBll().getShow(title))
                                    .getMainPanel());

            }
        });
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String title = (String)listShows.getSelectedValue();

                if(title == null || title.equals("Empty"))
                    UserInterface.showMessage(UserInterface.searchShowFrame, "Not a valid selection");
                else {

                    UserInterface.getBll().deleteShow(title);
                    UserInterface.showMessage(UserInterface.searchShowFrame, "Deleted show: " + title);
                    listModel.setElementAt("Empty", 0);

                }
            }
        });
        JSONFormatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String title = (String)listShows.getSelectedValue();

                if(title == null || title.equals("Empty"))
                    UserInterface.showMessage(UserInterface.searchShowFrame, "Not a valid selection");
                else {

                    UserInterface.getBll().printTicketsJSON(title);
                    UserInterface.showMessage(UserInterface.searchShowFrame, "File saved");
                }
            }
        });
        CSVFormatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String title = (String)listShows.getSelectedValue();

                if(title == null || title.equals("Empty"))
                    UserInterface.showMessage(UserInterface.searchShowFrame, "Not a valid selection");
                else {

                    UserInterface.getBll().printTicketsCSV(title);
                    UserInterface.showMessage(UserInterface.searchShowFrame, "File saved");
                }
            }
        });
    }

    public JPanel getMainPanel(){

        return mainPanel;
    }
}
