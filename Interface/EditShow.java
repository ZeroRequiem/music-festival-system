package Interface;


import Model.Shows;
import Validation.Validation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

public class EditShow {

    private JTextField titleField;
    private JList<String> listArtists;
    private JButton addArtistButton;
    private JList<String> listGenres;
    private JButton addGenreButton;
    private JTextField yearField;
    private JComboBox<String> daysComboBox;
    private JComboBox<String> timeComboBox;
    private JComboBox<String> monthsComboBox;
    private JButton saveButton;
    private JPanel mainPanel;
    private JList<String> selectedArtists;
    private JList<String> selectedGenres;
    private JScrollPane selectedArtistScroll;
    private JScrollPane selectedGenresScroll;
    private JScrollPane genreScroll;
    private JScrollPane artistScroll;
    private JButton removeArtistButton;
    private JButton removeGenreButton;
    private JTextField nrTicketsField;
    private String oldTitle;

    private String[] months = new String[]{"January", "February", "March", "April", "May", "June", "July",
            "August", "September", "October", "November", "December"};

    private String[] days = new String[]{"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13",
            "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24",
            "25", "26", "27", "28", "29", "30", "31"};

    private String[] hours = new String[]{"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
            "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};

    private DefaultListModel<String> modelSelectedGenres = new DefaultListModel<String>();
    private DefaultListModel<String> modelSelectedArtists = new DefaultListModel<String>();

    private void addItem(String selectedItem, DefaultListModel<String> listModel) throws IOException{

        if (selectedItem == null || selectedItem.equals("empty") || listModel.contains(selectedItem))
            throw new IOException("Invalid selection");
        else
            listModel.addElement(selectedItem);
    }

    private void removeItem(String selectedItem, DefaultListModel<String> listModel) throws IOException{

        if (selectedItem == null || selectedItem.equals("empty"))
            throw new IOException("Invalid Selection");
        else
            listModel.removeElement(selectedItem);

    }

    public EditShow(Shows givenShow){

        this();

        try {
    
            oldTitle = givenShow.getTitle();
            titleField.setText(givenShow.getTitle());
            nrTicketsField.setText(String.valueOf(givenShow.getMaxNrTickets()));

            for (String currGenre : UserInterface.getBll().getGenres(givenShow.getId()))
                addItem(currGenre, modelSelectedGenres);

            for(String currArtist : UserInterface.getBll().getArtists(givenShow.getId()))
                addItem(currArtist, modelSelectedArtists);

            LocalDateTime dateTime = givenShow.getDate().toLocalDateTime();

            yearField.setText(String.valueOf(dateTime.getYear()));
            monthsComboBox.setSelectedItem(months[dateTime.getMonthValue() - 1]);
            daysComboBox.setSelectedItem(days[dateTime.getDayOfMonth() - 1]);
            timeComboBox.setSelectedItem(hours[dateTime.getHour()]);

        }catch (IOException exc){
            UserInterface.showMessage(UserInterface.editShowFrame, exc.getMessage());
        }
    }

    public EditShow() {

        nrTicketsField.setText("20000");

        monthsComboBox.setPreferredSize(new Dimension(50, 20));
        daysComboBox.setPreferredSize(new Dimension(50, 20));

        DefaultListModel<String> modelArtists = new DefaultListModel<String>();
        List<String> result = UserInterface.getBll().getAllArtists();

        for(String curr: result)
            modelArtists.addElement(curr);

        listArtists = new JList<>();
        listArtists.setModel(modelArtists);
        artistScroll.setViewportView(listArtists);

        DefaultListModel<String> modelGenres = new DefaultListModel<String>();
        result = UserInterface.getBll().getAllGenres();

        for(String curr: result)
            modelGenres.addElement(curr);

        listGenres = new JList<String>();
        listGenres.setModel(modelGenres);
        genreScroll.setViewportView(listGenres);

        selectedArtists = new JList<>();
        selectedArtists.setModel(modelSelectedArtists);
        selectedArtistScroll.setViewportView(selectedArtists);

        selectedGenres = new JList<String>();
        selectedGenres.setModel(modelSelectedGenres);
        selectedGenresScroll.setViewportView(selectedGenres);

        addArtistButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {

                    String selectedArtist = listArtists.getSelectedValue();
                    addItem(selectedArtist, modelSelectedArtists);

                }catch (IOException exc){
                    UserInterface.showMessage(UserInterface.editShowFrame, exc.getMessage());
                }
            }
        });
        addGenreButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {

                    String selectedGenre = listGenres.getSelectedValue();
                    addItem(selectedGenre, modelSelectedGenres);

                }catch (IOException exc) {
                    UserInterface.showMessage(UserInterface.editShowFrame, exc.getMessage());
                }

            }
        });
        removeArtistButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {

                    String selectedArtist = selectedArtists.getSelectedValue();
                    removeItem(selectedArtist, modelSelectedArtists);

                }catch (IOException exc){
                    UserInterface.showMessage(UserInterface.editShowFrame, exc.getMessage());
                }
            }
        });
        removeGenreButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {

                    String selectedGenre = selectedGenres.getSelectedValue();
                    removeItem(selectedGenre, modelSelectedGenres);

                }catch (IOException exc){
                    UserInterface.showMessage(UserInterface.editShowFrame, exc.getMessage());
                }
            }
        });
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String title = titleField.getText();

                if(title.equals("")){

                    UserInterface.showMessage(UserInterface.editShowFrame, "Title field can't be empty");
                    return;
                }

                try {

                    int nrTickets = Validation.validateInteger(nrTicketsField.getText());
                    String year = yearField.getText();
                    String month = String.valueOf(monthsComboBox.getSelectedIndex() + 1);
                    String day = (String) daysComboBox.getSelectedItem();
                    String hour = (String) timeComboBox.getSelectedItem();
                    
                    UserInterface.getBll().updateShow(oldTitle, title, year, month, day, hour, nrTickets);
                    UserInterface.getBll().editShowGenres(title, modelSelectedGenres);
                    UserInterface.getBll().editShowArtists(title, modelSelectedArtists);
                    
                    UserInterface.showMessage(UserInterface.editShowFrame, "Updated show with name:" + title);

                }catch(IOException exc){

                    UserInterface.showMessage(UserInterface.editShowFrame, exc.getMessage());
                }
            }
        });
    }

    public JPanel getMainPanel(){return mainPanel;}

    private void createUIComponents() {

        monthsComboBox = new JComboBox<String>(months);
        daysComboBox = new JComboBox<>(days);
        timeComboBox = new JComboBox<>(hours);

    }
}
