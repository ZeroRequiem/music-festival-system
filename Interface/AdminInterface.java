package Interface;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdminInterface {

    private JButton createCashierButton;
    private JPanel panel1;
    private JButton searchCashierButton;
    private JButton createShowButton;
    private JButton searchShowButton;

    public AdminInterface() {
        createCashierButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                UserInterface.openTemporaryWindow(UserInterface.createCashierFrame, new CreateCashier().getMainPanel());
            }
        });

        searchCashierButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                UserInterface.openTemporaryWindow(UserInterface.searchCashierFrame, new SearchCashier().getMainPanel());
            }
        });
        createShowButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                UserInterface.openTemporaryWindow(UserInterface.createShowFrame, new CreateShow().getMainPanel());
            }
        });
        searchShowButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                UserInterface.openTemporaryWindow(UserInterface.searchShowFrame, new SearchShow().getMainPanel());
            }
        });
    }

    public JPanel getPanel1(){return panel1;}
}
