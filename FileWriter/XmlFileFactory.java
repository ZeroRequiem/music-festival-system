package FileWriter;

public class XmlFileFactory implements FileFactory {

    @Override
    public FileType createFile() {
        return new XmlFile();
    }
}
