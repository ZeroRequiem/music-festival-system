package FileWriter;

import Auxi.FILE_TYPE;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import com.opencsv.CSVWriter;
import Model.Reservations;

public class CsvFile extends FileType{


    @Override
    public void setFileType() {

        file_type = FILE_TYPE.CSV;
    }

    @Override
    public void generateFile(String fileName, List<Object> content) throws Exception{

        if(content == null || content.size() == 0)
            throw new IOException("List is empty");

        if(content.get(0) instanceof Reservations){

            CSVWriter csvWriter = new CSVWriter(new FileWriter(fileName + ".csv"));
            csvWriter.writeNext(new String[]{"id", "Name", "nrSeats"});

            content.forEach(obj -> {

                Reservations objRes = (Reservations) obj;
                csvWriter.writeNext(new String[]{String.valueOf(objRes.getId()), objRes.getName(), String.valueOf(objRes.getNumberOfSeats())});
            });

            csvWriter.close();
        }
    }

    public CsvFile(){

        setFileType();
    }

}
