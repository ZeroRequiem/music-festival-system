package FileWriter;

import Auxi.FILE_TYPE;
import Model.Reservations;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class JsonFile extends FileType{

    @Override
    public void setFileType() {
        file_type = FILE_TYPE.JSON;
    }

    @Override
    public void generateFile(String fileName, List<Object> content) throws Exception {

        if(content == null || content.size() == 0)
            throw new IOException("List is empty");

        if(content.get(0) instanceof Reservations) {

            FileWriter fileWriter = new FileWriter(fileName + ".json");
            JSONObject jsonObject = new JSONObject();
            JSONArray jsonArray = new JSONArray();

            jsonObject.put("Tickets", fileName);

            content.forEach(obj -> {

                try {

                    Reservations objRes = (Reservations) obj;
                    JSONObject res = new JSONObject();

                    res.put("id", objRes.getId());
                    res.put("name", objRes.getName());
                    res.put("nrSeats", objRes.getNumberOfSeats());

                    jsonArray.add(res);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            jsonObject.put("Reservations: ", jsonArray);

            ObjectMapper mapper = new ObjectMapper();
            String initJson = jsonObject.toJSONString();

            fileWriter.write(
                            mapper.writerWithDefaultPrettyPrinter()
                                    .writeValueAsString(mapper.readTree(initJson)));
            fileWriter.close();
        }
    }
}
