package FileWriter;
import Auxi.FILE_TYPE;

public interface FileFactory {

    public FileType createFile();
}
