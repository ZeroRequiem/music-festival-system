package FileWriter;

import Auxi.FILE_TYPE;
import Model.Reservations;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class XmlFile extends FileType{


    @Override
    public void setFileType() {

        file_type = FILE_TYPE.XML;
    }

    @Override
    public void generateFile(String fileName, List<Object> content) throws Exception {

        if(content == null || content.size() == 0)
            throw new IOException("List is empty");

        if(content.get(0) instanceof Reservations){

            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            Element rootElement = document.createElement("tickets");
            document.appendChild(rootElement);

            content.forEach(obj -> {

                Reservations objRes = (Reservations) obj;

                Element reservation = document.createElement("reservation");
                Element reservationName = document.createElement("name");
                reservationName.setTextContent(objRes.getName());

                Element nrSeats = document.createElement("nrSeats");
                nrSeats.setTextContent(String.valueOf(objRes.getNumberOfSeats()));

                reservation.setAttribute("id", String.valueOf(objRes.getId()));
                reservation.appendChild(reservationName);
                reservation.appendChild(nrSeats);

                rootElement.appendChild(reservation);
            });

            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File(fileName + ".xml"));

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.transform(source, result);
        }
    }

    public XmlFile() {

        setFileType();
    }
}
