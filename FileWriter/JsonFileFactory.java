package FileWriter;

public class JsonFileFactory implements FileFactory{

    @Override
    public FileType createFile() {
        return new JsonFile();
    }
}
