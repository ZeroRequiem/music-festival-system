package FileWriter;

public class CsvFileFactory implements FileFactory{

    @Override
    public FileType createFile() {
        return new CsvFile();
    }
}
