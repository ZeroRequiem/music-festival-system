package BLL;

import Auxi.FILE_TYPE;
import Auxi.Status;
import DataAccess.*;
import FileWriter.*;
import Model.*;
import Validation.Validation;

import javax.swing.*;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

public class BLL {

    private ArtistsDAO artistsAccess;
    private GenresDAO genresAccess;
    private ReservationsDAO reservationsAccess;
    private UsersDAO usersAccess;
    private ShowsDAO showsAccess;
    private ShowArtistsDAO showArtistsAccess;
    private ShowGenresDAO showGenresAccess;

    private Base64.Encoder encoder = Base64.getEncoder();
    private Base64.Decoder decoder = Base64.getDecoder();

    public BLL() {

        this.artistsAccess = new ArtistsDAO();
        this.genresAccess = new GenresDAO();
        this.reservationsAccess = new ReservationsDAO();
        this.usersAccess = new UsersDAO();
        this.showsAccess = new ShowsDAO();
        this.showArtistsAccess = new ShowArtistsDAO();
        this.showGenresAccess = new ShowGenresDAO();
    }

    public String decode(String password){

        return new String(decoder.decode(password.getBytes()));
    }

    public int getUserStatus(String username, String password) throws IOException {

        String encodedPassword = encoder.encodeToString(password.getBytes());
        return usersAccess.getStatusUser(username, encodedPassword);
    }

    public void createCashier(String name, String password){

        String encodedPassword = new String(encoder.encode(password.getBytes()));
        usersAccess.createCashier(name, encodedPassword);
    }

    public String[] searchCashier(String name){

        List<String> result = usersAccess.getCashiers(name);
        String[] finalResult = null;

        if(result.size() == 0)
            finalResult = new String[]{"Empty"};
        else{

            finalResult = new String[result.size()];
            finalResult = result.toArray(finalResult);
        }


        return finalResult;
    }

    public String[] searchShow(String title){

        List<String> result = showsAccess.getShows(title);
        String[] finalResult = null;

        if(result.size() == 0)
            finalResult = new String[]{"Empty"};
        else{

            finalResult = new String[result.size()];
            finalResult = result.toArray(finalResult);
        }

        return finalResult;
    }

    public List<String> getAllArtists(){

        return artistsAccess.selectAll()
                            .stream()
                            .map(Artists::getName)
                            .collect(Collectors.toList());
    }

    public List<String> getAllGenres(){

        return genresAccess.selectAll()
                            .stream()
                            .map(Genres::getName)
                            .collect(Collectors.toList());
    }

    public void editCashier(String oldName, String newName, String newPassword){

        int id = usersAccess.getID("name", oldName);
        String encodedPassword = encoder.encodeToString(newPassword.getBytes());

        usersAccess.update(id, "name", newName);
        usersAccess.update(id, "password", encodedPassword);
    }

    public String getPassword(String name){

        return usersAccess.selectAll("name", name).get(0).getPassword();
    }

    public void deleteCashier(String name){

        usersAccess.delete("name", name, false);
        usersAccess.delete("name", name, false);
    }

    public void insertShow(String title, ListModel<String> artists, ListModel<String> genres, String year, String month, String day, String hour, int nrTickets) throws IOException{

        try {

            Timestamp timestamp = Validation.validateDate(year, month, day, hour);

            int idShow = showsAccess.getMaxId() + 1;
            Shows showToBeInserted = new Shows(idShow, title, timestamp, nrTickets);

            showsAccess.insert(showToBeInserted);

            for(int index = 0; index < artists.getSize(); index++){

                int idArtist = artistsAccess.getID("name", artists.getElementAt(index));
                ShowArtists showArtist = new ShowArtists(idShow, idArtist);
                showArtistsAccess.insert(showArtist);
            }

            for(int index = 0; index < genres.getSize(); index++){

                int idGenre = genresAccess.getID("name", genres.getElementAt(index));
                ShowGenres showGenre = new ShowGenres(idShow, idGenre);
                showGenresAccess.insert(showGenre);
            }

        }catch (Exception exc){
            throw new IOException("Invalid date");
        }
    }

    public List<String> getGenres(int idShow){

        return showGenresAccess.selectAll("idShow", idShow).stream()
                                                            .map(x -> genresAccess.findById(x.getIdGenre()).getName())
                                                            .collect(Collectors.toList());
    }

    public List<String> getArtists(int idShow){

        return showArtistsAccess.selectAll("idShow", idShow).stream()
                                                            .map(x -> artistsAccess.findById(x.getIdArtist()).getName())
                                                            .collect(Collectors.toList());
    }

    public void editShowGenres(String title, DefaultListModel<String> newGenres){

        int idShow = showsAccess.getID("title", title);
        showGenresAccess.delete("idshow", idShow, false);

        Arrays.asList(newGenres.toArray()).forEach(newGenre -> {

            int idGenre = genresAccess.getID("name", newGenre);
            showGenresAccess.insert(new ShowGenres(idShow, idGenre));
        });
    }

    public void editShowArtists(String title, DefaultListModel<String> newArtists){

        int idShow = showsAccess.getID("title", title);
        showArtistsAccess.delete("idshow", idShow, false);

        Arrays.asList(newArtists.toArray()).forEach(newArtist -> {

            int idArtist = artistsAccess.getID("name", newArtist);
            showArtistsAccess.insert(new ShowArtists(idShow, idArtist));
        });
    }

    public void updateShow(String oldTitle, String newTitle, String year, String month, String day, String hour, int nrMaxTickets) throws IOException{

        int idShow = getShow(oldTitle).getId();;
        Timestamp newDate = Validation.validateDate(year, month, day, hour);

        showsAccess.update(idShow, "title", newTitle);
        showsAccess.update(idShow, "date", newDate);
        showsAccess.update(idShow, "maxNrTickets", nrMaxTickets);
    }

    public Shows getShow(String title){

        return showsAccess.selectAll("title", title).get(0);
    }

    public Shows findShow(String title) throws IOException{

        List<Shows> result = showsAccess.selectAll("title", title);

        if(result.size() == 0)
            throw new IOException("Unable to find show");

        return result.get(0);
    }

    public void printTicketsXML(String title){

        try {

            int idShow = getShow(title).getId();
            List reservations = reservationsAccess.selectAll("idShow", idShow);

            XmlFileFactory xmlFileFactory = new XmlFileFactory();
            FileType file = xmlFileFactory.createFile();
            file.generateFile(title + String.valueOf(idShow), reservations);

        }catch (Exception exc){
            exc.printStackTrace();
        }

    }

    public void printTicketsCSV(String title){

        try{

            int idShow = getShow(title).getId();
            List reservations = reservationsAccess.selectAll("idShow", idShow);

            CsvFileFactory csvFileFactory = new CsvFileFactory();
            FileType file = csvFileFactory.createFile();
            file.generateFile(title + String.valueOf(idShow), reservations);

        }catch (Exception exc){
            exc.printStackTrace();
        }
    }

    public void printTicketsJSON(String title){

        try{

            int idShow = getShow(title).getId();
            List reservations = reservationsAccess.selectAll("idShow", idShow);

            JsonFileFactory jsonFileFactory = new JsonFileFactory();
            FileType file = jsonFileFactory.createFile();
            file.generateFile(title + String.valueOf(idShow), reservations);

        }catch (Exception exc){

            exc.printStackTrace();
        }
    }

    public void addReservation(String showName, String reservationName, int nrSeats) throws IOException{

        Shows show = getShow(showName);
        int nrTickets = show.getMaxNrTickets();

        if(nrTickets - nrSeats < 0)
            throw new IOException("Max number of tickets exceeded");
        else if(nrTickets - nrSeats >= 0){

            int idReservation = reservationsAccess.getMaxId() + 1;
            int idShow = show.getId();

            Reservations reservation = new Reservations(idReservation, reservationName, nrSeats, idShow);
            reservationsAccess.insert(reservation);

            showsAccess.update(idShow, "maxNrTickets", nrTickets - nrSeats);

            if(nrTickets - nrSeats == 0)
                throw new IOException("Last ticket sold, tickets have been sold out");
        }
    }

    public String[] searchReservation(String name){

        List<String> result = reservationsAccess.getReservations(name);
        String[] finalResult = null;

        if(result.size() == 0)
            finalResult = new String[]{"Empty"};
        else{

            finalResult = new String[result.size()];
            finalResult = result.toArray(finalResult);
        }

        return finalResult;
    }

    public void deleteReservation(String name){

        reservationsAccess.delete("name", name, false);
    }

    public void deleteShow(String title){

        showsAccess.delete("title", title, false);
    }

    public Reservations getReservation(String nameReservation){

        return reservationsAccess.selectAll().get(0);
    }

    public void editReservation(String oldName, String newName, int newNrSeats) throws IOException{

        Reservations reservation = reservationsAccess.selectAll("name", oldName).get(0);

        Shows show = showsAccess.findById(reservation.getIdShow());
        int diff = reservation.getNumberOfSeats() - newNrSeats;

        if(diff < 0){

            if(show.getMaxNrTickets() + diff < 0)
                throw new IOException("Max number of tickets exceeded");
            else if(show.getMaxNrTickets() + diff >= 0) {

                reservationsAccess.update(reservation.getId(), "name", newName);
                reservationsAccess.update(reservation.getId(), "numberOfSeats", newNrSeats);

                showsAccess.update(reservation.getIdShow(), "maxNrTickets", diff + show.getMaxNrTickets());

                if(show.getMaxNrTickets() - diff == 0)
                    throw new IOException("Last ticket sold, tickets have been sold out");
            }

        }else if(diff > 0){

            reservationsAccess.update(reservation.getId(), "name", newName);
            reservationsAccess.update(reservation.getId(), "numberOfSeats", newNrSeats);
            showsAccess.update(reservation.getIdShow(), "maxNrTickets", diff + show.getMaxNrTickets());
        }

    }

    public String[] getReservationsForShow(String showName) throws IOException{

        String[] finalResult = null;
        int idShow = showsAccess.getID("title", showName);

        List<String> result = reservationsAccess.selectAll("idShow", idShow)
                                                    .stream()
                                                    .map(x -> String.valueOf(x.getId()) +
                                                            "                       "
                                                            + x.getName() +
                                                            "                       "
                                                            + x.getNumberOfSeats())
                                                    .collect(Collectors.toList());

        if(result.size() == 0)
            throw new IOException("Unable to find reservations for show: " + showName);
        else{

            finalResult = new String[result.size()];
            finalResult = result.toArray(finalResult);
        }

        return finalResult;
    }
}
