package Validation;

import java.io.IOException;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;

public class Validation {

    public static int validateInteger(String in) throws IOException{

        try{

            return Integer.parseInt(in);
        }catch (Exception exc){

            throw new IOException("Invalid format for number of tickets field");
        }
    }

    public static Timestamp validateDate(String year, String month, String day, String hour) throws IOException {

        try{

            if(Integer.parseInt(month) < 10)
                month = "0" + month;

            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/uuuu").withResolverStyle(ResolverStyle.STRICT);
            LocalDate localDate = LocalDate.parse(day + "/" + month + "/" + year, dateTimeFormatter);
            String date = year + "-" + month + "-" + day + " " + hour + ":00:00";

            return Timestamp.valueOf(date);

        }catch (Exception exc){
            throw new IOException("Invalid date");
        }
    }
}
