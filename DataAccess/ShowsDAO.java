package DataAccess;

import Model.Shows;
import java.util.List;
import java.util.stream.Collectors;

public class ShowsDAO extends AbstractDAO<Shows>{

    public List<String> getShows(String title){

        return selectAll("title", title).stream()
                .map(Shows::getTitle)
                .collect(Collectors.toList());
    }
}
