package DataAccess;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <h1>Abstract Class for database interaction</h1>
 *
 * It is used as a bridge to perform basic operations on tables, the concept of reflection and generics were used to reduce the code.
 * @param <T> the generic type
 */
public class AbstractDAO<T> {

    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    protected final Class<T> type;

    @SuppressWarnings("unchecked")
    /**
     * <h1>Default constructor</h1>
     */
    public AbstractDAO(){ this.type = (Class<T>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]; }

    /**
     * <h1>Generate a delete query string</h1>
     * @param field the field based on which the deletion will be performed
     * @return query in string format
     */
    private String createDeleteQuery(String field){ return "delete from `" + type.getSimpleName() + "` where " + field + " = ?"; }

    /**
     * <h1>Generate a update query string</h1>
     * @param field the field which will be updated
     * @return update string
     */
    private String createUpdateQuery(String field){ return "update `" + type.getSimpleName() + "` set " + field + "= ? where id = ?"; }

    /**
     * <h1>Generate a insert query string</h1>
     * @param nrOfValues how many values will be inserted
     * @return insert query
     */
    private String createInsertQuery(int nrOfValues){

        String buffer = "(";

        for(int i = 0; i < nrOfValues; i++){

            buffer += "?";

            if(i != nrOfValues - 1)
                buffer += ",";
        }

        buffer += ")";

        return "insert into `" + type.getSimpleName() + "` values" + buffer;
    }

    /**
     * <h1>Generate a select query</h1>
     * @param field the field based on which selection is performed
     * @return select query
     */
    String createSelectQuery(String field){ return "SELECT * FROM `" + type.getSimpleName() + "` WHERE " + field + " = ?"; }

    /**
     * <h1>Select all entries based on conditions</h1>
     * @param field field based on which selection is performed
     * @param value the value of the specified field
     * @return list of objects obtained after executing the query if successful, null otherwise
     */
    public List<T> selectAll(String field, Object value){

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query;

        if(value == null)//don't select based on field
            query = "Select * from `" + type.getSimpleName() + "`";
        else
            query = createSelectQuery(field);

        try{

            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);

            if(value != null) {//set statement placeholders based on values

                Object[] values = {value};
                setStatement(statement, values);
            }

            resultSet = statement.executeQuery();

            return createObjects(resultSet);

        }catch (SQLException e){
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findByID " + e.getMessage());
        }finally { ConnectionFactory.closeAll(resultSet, statement, connection); }

        return null;
    }

    /**
     * <h1>Create objects based of entries from the database</h1>
     * @param resultSet values from data base
     * @return list of entries if successful, null otherwise
     */
    List<T> createObjects(ResultSet resultSet){

        List<T> list = new ArrayList<T>();

        try{

            while(resultSet.next()){//entries left

                T instance = type.newInstance();//create a new object of type 'T'

                for(Field field : type.getDeclaredFields()){//for each declared field of object of type 'T'

                    Object value = resultSet.getObject(field.getName());//get value from result set
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);//generate property descriptor
                    Method method = propertyDescriptor.getWriteMethod();//generate write ( set ) method
                    method.invoke(instance, value);//invoke method to set the current field to given value

                }
                list.add(instance);//add object to the list of entries
            }

            return list;

        }catch (Exception e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:create objects " + e.getMessage());
        }

        return null;

    }

    /**
     * <h1>Insert a new entry</h1>
     * @param t entry
     * @return created entry if successful, null otherwise
     */
    public T insert(T t){

        Connection connection = null;
        PreparedStatement statement = null;

        String query = createInsertQuery(type.getDeclaredFields().length);

        try{

            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            Object[] values = new Object[type.getDeclaredFields().length];
            int index = 0;

            for (Field field : type.getDeclaredFields()) {//for all fields

                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                Method method = propertyDescriptor.getReadMethod();//get read method
                Object value = method.invoke(t);//invoke it to obtain the value saved

                values[index++] = value;//used to set the statement place holders
            }

            setStatement(statement, values);
            statement.executeUpdate();

            return t;

        }catch (Exception e){
            LOGGER.log(Level.WARNING, type.getName() + "DAO:insert new data " + e.getMessage());
        }finally { ConnectionFactory.closeAll(statement, connection); }

        return null;
    }

    /**
     * <h1>Update entry</h1>
     * @param id ID of the entry to be updated
     * @param field field based on which the update is performed
     * @param newValue the new value to be set
     */
    public void update(int id, String field, Object newValue){

        Connection connection = null;
        PreparedStatement statement = null;

        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(createUpdateQuery(field));

            Object[] values = {newValue};
            setStatement(statement, values);

            statement.setInt(2, id);
            statement.executeUpdate();

        }catch (Exception e){
            LOGGER.log(Level.WARNING, type.getName() + "DAO:update field " + e.getMessage());}
        finally { ConnectionFactory.closeAll(statement, connection);}
    }

    /**
     * <h1>Delete entry</h1>
     * @param field field based on which the deletion is performed
     * @param value value to be checked
     * @param deleteAll flag in case we want to delete all entries
     */
    public void delete(Object field, Object value, boolean deleteAll){

        Connection connection = null;
        PreparedStatement statement = null;

        String query;

        if(deleteAll)//delete all entries
            query = "delete from `" + type.getSimpleName() + "`";
        else//deletion based on condition
            query = createDeleteQuery(field.toString());

        try{

            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);

            if(!deleteAll) {//set place holders

                Object[] values = {value};
                setStatement(statement, values);
            }

            statement.executeUpdate();

        }catch (Exception e){

            LOGGER.log(Level.WARNING, type.getName() + "DAO:delete entry " + e.getMessage());
        }finally {
            ConnectionFactory.closeAll(statement, connection);
        }
    }

    /**
     * <h1>Used for setting place holders of statement</h1>
     * @param statement statement to be set
     * @param values values to be set
     * @throws SQLException in case of SQLException
     */
    void setStatement(PreparedStatement statement, Object[] values) throws SQLException{

        int paramIndex = 1;

        for (Object value : values) {//for all values

            if (value instanceof Integer)//check what type of instance is to know what method to use to set the statement
                statement.setInt(paramIndex, Integer.parseInt(value.toString()));

            if (value.getClass().isAssignableFrom(String.class))
                statement.setString(paramIndex, value.toString());

            if (value instanceof Float)
                statement.setFloat(paramIndex, (float)value);

            if(value instanceof Boolean)
                statement.setBoolean(paramIndex, (boolean)value);

            if(value instanceof Date)
                statement.setDate(paramIndex, (Date)value);

            if(value instanceof Timestamp)
                statement.setTimestamp(paramIndex, (Timestamp)value);

            paramIndex++;
        }
    }

    /**
     * <h1>Find entry based on id</h1>
     * @param id the id of the entry
     * @return object of entry if successful, null otherwise
     */
    public T findById(int id){

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        String query = createSelectQuery("id");

        try{

            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            if(!resultSet.isBeforeFirst())//result set is empty, entry couldn't be found
                return null;

            return createObjects(resultSet).get(0);

        }catch (SQLException e){
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findByID " + e.getMessage());
        }finally { ConnectionFactory.closeAll(resultSet, statement, connection); }

        return null;
    }

    /**
     * <h1>Used for getting ID of entry based on one or more fields</h1>
     * @param query the query to be executed
     * @param values the values to be checked
     * @return ID of entry if successful, -1 otherwise
     */
    private int getQuery(String query, Object[] values){

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try{

            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);

            if(values != null)
                setStatement(statement, values);

            resultSet = statement.executeQuery();

            if(resultSet.next())//if not empty
                return resultSet.getInt(1);
            else
                return -1;//not found

        }catch (Exception e){
            LOGGER.log(Level.WARNING, type.getName() + "DAO:get ID " + e.getMessage());
        }finally {
            ConnectionFactory.closeAll(resultSet, statement, connection);
        }

        return -1;
    }

    /**
     * <h1>Select all entries</h1>
     * @return list of objects found in the specified table if successful, null otherwise
     */
    public List<T> selectAll(){ return selectAll(null, null); }

    /**
     * <h1>Get ID of entry based on a single condition</h1>
     * @param field field based on which selection is performed
     * @param value the value to be checked
     * @return ID of entry if successful, -1 otherwise
     */
    public int getID(String field, Object value){

        Object[] values = {value};

        return this.getQuery("select id from `" + type.getSimpleName() + "` where " + field + " = ?", values);
    }

    /**
     * <h1>Get ID of entry based on 2 conditions</h1>
     * @param field1 first field based on which selection is performed
     * @param field2 second field based on which selection is performed
     * @param value1 first value to be checked
     * @param value2 second value to be checked
     * @return  ID of entry if successful, -1 otherwise
     */
    public int getID(String field1, String field2, Object value1, Object value2){

        Object[] values = {value1, value2};

        return this.getQuery("select id from `" + type.getSimpleName() + "` where " + field1 + " = ? and " + field2 + " = ?", values);
    }

    /**
     * <h1>Get max id</h1>
     * @return max id if successful, -1 otherwise
     */
    public int getMaxId(){ return this.getQuery("select max(id) from `" + type.getSimpleName() + "`", null); }

}

