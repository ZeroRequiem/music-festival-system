package DataAccess;

import Auxi.Status;
import Model.Users;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class UsersDAO extends AbstractDAO<Users>{

    public int getStatusUser(String username, String password) throws IOException {

        int idUser = getID("name", "password", username, password);

        if(idUser != Status.NOT_FOUND){

            Users user = findById(idUser);

            if(user.isIsAdmin())
                return Status.ADMIN;
            else
                return Status.CASHIER;

        }else
            throw new IOException("User not found");
    }

    public void createCashier(String name, String password){

         Users cashier = new Users(getMaxId() + 1, name, password, false);

         insert(cashier);
    }

    public List<String> getCashiers(String name){

        return selectAll("name", name).stream()
                                            .map(Users::getName)
                                            .collect(Collectors.toList());
    }
}
