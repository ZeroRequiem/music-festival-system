package DataAccess;

import Model.Reservations;

import java.util.List;
import java.util.stream.Collectors;

public class ReservationsDAO extends AbstractDAO<Reservations>{

    public List<String> getReservations(String name){

        return selectAll("name", name).stream()
                                            .map(Reservations::getName)
                                            .collect(Collectors.toList());
    }
}
