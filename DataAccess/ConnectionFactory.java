package DataAccess;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <h1>Connection to the data base</h1>
 */
public class ConnectionFactory {

    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/untold?characterEncoding=latin1&useConfigs=maxPerformance";
    private static final String USER = "root";
    private static final String PASS = "root";

    private static ConnectionFactory singleInstance = new ConnectionFactory();

    private ConnectionFactory(){

        try{

            Class.forName(DRIVER);

        }catch (ClassNotFoundException e){

            e.printStackTrace();
        }
    }

    /**
     * Create connection based on data base URL, username and password
     * @return connection to data base
     */
    private Connection createConnection(){

        Connection connection = null;

        try {

            connection = DriverManager.getConnection(DBURL, USER, PASS);

        }catch (SQLException e){

            LOGGER.log(Level.WARNING, "An error occured while trying to connect to the database");
            e.printStackTrace();
        }

        return connection;
    }

    /**
     *
     * @return connection to data base
     */
    public static Connection getConnection(){

        return singleInstance.createConnection();
    }

    /**
     * @param connection close the connection
     */
    public static void close(Connection connection){

        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.log(Level.WARNING, "An error occured while trying to close the connection");
            }
        }
    }

    /**
     * @param statement close the statement
     */
    public static void close(Statement statement){

        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.log(Level.WARNING, "An error occured while trying to close the statement");
            }
        }
    }

    /**
     * @param resultSet close the result set
     */
    public static void close(ResultSet resultSet){

        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                LOGGER.log(Level.WARNING, "An error occurred while trying to close the ResultSet");
            }
        }
    }

    /**
     * Used to close multiple things at once
     * @param statement close statement
     * @param connection close connection
     */
    public static void closeAll(Statement statement, Connection connection){

        close(statement);
        close(connection);
    }

    /**
     * @param resultSet close result set
     * @param statement close statement
     * @param connection close connection
     */
    public static void closeAll(ResultSet resultSet, Statement statement, Connection connection){

        close(connection);
        close(resultSet);
        close(statement);
    }

}
