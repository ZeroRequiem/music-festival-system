package Model;

public class ShowGenres {

    private int idShow;
    private int idGenre;

    public ShowGenres(int idShow, int idGenre) {

        this.idShow = idShow;
        this.idGenre = idGenre;
    }

    public ShowGenres(){}

    public int getIdShow() {
        return idShow;
    }

    public void setIdShow(int idShow) {
        this.idShow = idShow;
    }

    public int getIdGenre() {
        return idGenre;
    }

    public void setIdGenre(int idGenre) {
        this.idGenre = idGenre;
    }
}
