package Model;

public class ShowArtists {

    private int idShow;
    private int idArtist;

    public ShowArtists(int idShow, int idArtist) {

        this.idShow = idShow;
        this.idArtist = idArtist;
    }

    public ShowArtists(){}

    public int getIdShow() {
        return idShow;
    }

    public void setIdShow(int idShow) {
        this.idShow = idShow;
    }

    public int getIdArtist() {
        return idArtist;
    }

    public void setIdArtist(int idArtist) {
        this.idArtist = idArtist;
    }
}
