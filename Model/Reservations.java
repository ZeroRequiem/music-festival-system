package Model;

public class Reservations {

    private int id;
    private String name;
    private int numberOfSeats;
    private int idShow;

    public Reservations(int id, String name, int numberOfSeats, int idShow) {

        this.id = id;
        this.name = name;
        this.numberOfSeats = numberOfSeats;
        this.idShow = idShow;
    }

    public Reservations(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public int getIdShow() {
        return idShow;
    }

    public void setIdShow(int idShow) {
        this.idShow = idShow;
    }
}
