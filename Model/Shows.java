package Model;

import java.sql.Timestamp;

public class Shows {

    private int id;
    private String title;
    private Timestamp date;
    private int maxNrTickets;

    public Shows(){}

    public Shows(int id, String title, Timestamp date, int maxNrTickets) {

        this.id = id;
        this.title = title;
        this.date = date;
        this.maxNrTickets = maxNrTickets;
    }

    public Shows(int id, String title, Timestamp timestamp){
        this(id, title, timestamp, 20000);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public int getMaxNrTickets() {
        return maxNrTickets;
    }

    public void setMaxNrTickets(int maxNrTickets) {
        this.maxNrTickets = maxNrTickets;
    }
}
